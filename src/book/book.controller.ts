import { Body, Controller, Delete, Get, Headers, Param, Post, Put } from '@nestjs/common';
import { BookService } from './book.service';
import { Book } from '../model/Book';

@Controller()
export class BookController {
	constructor(private bookService: BookService) {
	}

	@Get()
	public getAllBooks() {
		return this.bookService.getAllBooks();
	}

	@Get('/:id')
	public getBookByID(@Param('id') id: string) {
		return this.bookService.findBookByID(id);
	}

	@Post()
	public async addBook(@Body() book: Book, @Headers() headers: any) {
		const newBook = await this.bookService.addBook(book);
		await this.bookService.notifyCommunityOfNewBook((newBook as any)._id, headers.authorization);
		return newBook;
	}

	@Put('/:id')
	public editBook(@Param('id') id: string, @Body() book: Partial<Book>) {
		return this.bookService.editBook(id, book);
	}

	/*@Put('/:id/related')
	public addRelatedBook(@Param('id') id: string, @Body() related: { books: Array<Ref<Book>> }) {
		return this.bookService.addRelatedBook(id, related.books);
	}*/

	@Delete('/:id')
	public deleteBook(@Param('id') id: string) {
		return this.bookService.deleteBook(id);
	}
}
