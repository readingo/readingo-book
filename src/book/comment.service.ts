import { Injectable } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { Book } from '../model/Book';
import { ReturnModelType } from '@typegoose/typegoose';
import { Comment } from '../model/Comment';
import { CopyAllProperties } from '../utils/CopyAllProperties';

@Injectable()
export class CommentService {
	constructor(
		@InjectModel(Book) private readonly bookModel: ReturnModelType<typeof Book>,
		@InjectModel(Comment) private readonly commentModel: ReturnModelType<typeof Comment>,
	) {
	}

	async addCommentToBook(bookID: string, commentDTO: Partial<Comment>): Promise<any> {
		const comment = new this.commentModel();
		CopyAllProperties(commentDTO, comment, true);
		comment.postedAt = new Date();
		comment.likes = 0;
		await comment.save();
		await this.bookModel.findByIdAndUpdate(bookID, {
			$push: {
				comments: comment,
			},
		});
		return comment;
	}

	async getCommentForBookPage(bookID: string, page: number) {
		return await this.bookModel.findById(bookID, { comments: 1 }).populate({ path: 'comments', match: { page } }).exec();
	}

	async removeCommentFromBook(bookID: string, commentID: string) {
		await this.commentModel.findByIdAndDelete(commentID);
		return this.bookModel.findByIdAndUpdate(bookID, {
			$pull: {
				comments: commentID,
			},
		});
	}
}
