import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { CommentService } from './comment.service';
import { Comment } from '../model/Comment';

@Controller()
export class CommentController {
	constructor(private commentService: CommentService) {
	}

	@Get('/:id/comment/:page')
	public getCommentForBookPage(@Param('id') id: string, @Param('page') page: number) {
		return this.commentService.getCommentForBookPage(id, page);
	}

	@Post('/:id/comment')
	public addCommentToBook(@Param('id') id: string, @Body() comment: Comment) {
		return this.commentService.addCommentToBook(id, comment);
	}

	@Delete('/:id/comment/:commentID')
	public removeCommentFromBook(@Param('id') id: string, @Param('commentID') commentID: string) {
		return this.commentService.removeCommentFromBook(id, commentID);
	}
}
