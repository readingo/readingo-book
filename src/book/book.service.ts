import { Inject, Injectable } from '@nestjs/common';
import { Book } from '../model/Book';
import { validateOrReject } from 'class-validator';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { ErrorObject, ResultObject, SuccessObject } from '../model/ResultObject';
import axios from 'axios';

@Injectable()
export class BookService {
	constructor(
		@InjectModel(Book) private readonly bookModel: ReturnModelType<typeof Book>,
		@Inject('COMMUNITY_SERVICE_URL') private readonly communityURL: string,
	) {
	}

	public async getAllBooks(): Promise<Array<Book>> {
		return await this.bookModel.find({}).exec();
	}

	public async findBookByID(id: string): Promise<Book | ErrorObject> {
		return await this.bookModel.findById(id).exec();
	}

	public async addBook(book: Book): Promise<Book> {
		return await new Promise(async (resolve, reject) => {
			await validateOrReject(book, {}).catch(() => reject(new ErrorObject('Invalid book')));
			const newBook = new this.bookModel(book);
			await newBook.save().catch(() => reject(new ErrorObject('Cannot save book')));
			resolve(newBook);
		});
	}

	public async editBook(id: string, newBook: Partial<Book>): Promise<Book> {
		const foundBook = await this.bookModel.findById(id).exec();
		const bookToAdd = Object.assign(foundBook, newBook);
		await bookToAdd.save();
		return bookToAdd as Book;
	}

	public async deleteBook(id: string): Promise<ResultObject> {
		return await this.bookModel.findByIdAndDelete(id).exec()
			.then(() => new SuccessObject())
			.catch(e => new ErrorObject(e));
	}

	public notifyCommunityOfNewBook(book: string, jwt: string) {
		return axios.post(`http://${this.communityURL}:9002/book`, { book, jwt });
	}
}
