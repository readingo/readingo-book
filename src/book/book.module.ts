import { Module } from '@nestjs/common';
import { BookService } from './book.service';
import { BookController } from './book.controller';
import { Book } from '../model/Book';
import { TypegooseModule } from 'nestjs-typegoose';
import { CommentService } from './comment.service';
import { CommentController } from './comment.controller';
import { Comment } from '../model/Comment';

@Module({
	imports: [
		TypegooseModule.forFeature([Book, Comment]),
	],
	providers: [
		BookService,
		CommentService,
		{
			provide: 'COMMUNITY_SERVICE_URL',
			useValue: process.env.NODE_ENV === 'development' ? 'localhost' : 'localhost',
		},
	],
	controllers: [
		BookController,
		CommentController,
	],
})
export class BookModule {
}
