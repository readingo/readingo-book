import { IsArray, IsDefined, IsString, ValidateNested } from 'class-validator';
import { prop, Ref } from '@typegoose/typegoose';

export class Character {
	@prop() @IsString() @IsDefined()
	public name: string;
	@prop() @IsString() @IsDefined()
	public role: string;
	@prop() @IsArray({ each: true }) @ValidateNested({ each: true })
	public relationships?: Array<Relation>;
}

export class Relation {
	@prop({ ref: Character })
	relative: Ref<Character>;
	@prop()
	type: string;
}
