import { prop } from '@typegoose/typegoose';
import { IsDate, IsDefined, IsInt, IsNumber, IsString, Min } from 'class-validator';

export class Comment {
	@prop({ required: true }) @IsNumber() @IsDefined()
	public page: number;

	@prop({ required: true }) @IsString() @IsDefined()
	public postedBy: string;

	@prop({ required: true }) @IsString() @IsDefined()
	public contents: string;

	@prop({ required: true }) @IsDate() @IsDefined()
	public postedAt: Date;

	@prop({ required: true }) @IsInt() @Min(0) @IsDefined()
	public likes: number;
}
