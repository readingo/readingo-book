export interface ResultObject {
	readonly success: boolean;
}

export class ErrorObject implements ResultObject {
	constructor(
		public reason: string,
		public readonly success: boolean = false,
	) {
	}
}

export class SuccessObject implements ResultObject {
	constructor(
		public readonly success: boolean = true,
	) {
	}
}
