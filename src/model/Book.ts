import { Character } from './Character';
import { IsArray, IsDefined, IsFQDN, IsNumber, IsPositive, IsString, ValidateNested } from 'class-validator';
import { arrayProp, prop, Ref } from '@typegoose/typegoose';
import { Comment } from './Comment';

export class Book {
	@prop({ required: true }) @IsString() @IsDefined()
	public ISBN: string;

	@prop({ required: true }) @IsString() @IsDefined()
	public title: string;

	@prop() @IsString()
	public author?: string;

	@prop() @IsString()
	public description?: string;

	@prop() @IsString() @IsFQDN()
	public thumbnail?: string;

	@prop({ min: 0 }) @IsNumber() @IsPositive()
	public pageCount?: number;

	@prop({ min: 0 }) @IsNumber() @IsPositive()
	public releaseDate?: number;

	@arrayProp({ itemsRef: Character }) @IsArray() @ValidateNested({ each: true })
	public characters?: Array<Ref<Character>>;

	@arrayProp({ itemsRef: Book }) @IsArray() @ValidateNested({ each: true })
	public relatedBooks?: Array<Ref<Book>>;

	@arrayProp({ itemsRef: Comment }) @IsArray() @ValidateNested({ each: true })
	public comments: Array<Comment>;
}
