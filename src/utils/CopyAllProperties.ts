export const CopyAllProperties = <A, B>(from: A, to: B, force: boolean = false): void => {
	if (typeof from !== 'object' || typeof to !== 'object') {
		throw new Error('Can not copy properties of not objects');
	}

	Object.getOwnPropertyNames(from).forEach(key => {
		if (force || to.hasOwnProperty(key) && Object.getOwnPropertyDescriptor(to, key).writable) {
			to[key] = from[key];
		}
	});
};
