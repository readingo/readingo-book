import { Module } from '@nestjs/common';
import { BookModule } from './book/book.module';
import { TypegooseModule } from 'nestjs-typegoose';
import 'dotenv/config';

const { DB_HOST, DB_PORT, DB_NAME } = process.env;

@Module({
	imports: [
		TypegooseModule.forRoot(`mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false,
		}),
		BookModule,
	],
	controllers: [],
	providers: [],
})
export class AppModule {
}
