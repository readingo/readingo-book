FROM node:12-alpine
WORKDIR /app
COPY /dist dist
COPY /node_modules node_modules
EXPOSE 3000
CMD [ "node", "dist/main.js" ]
